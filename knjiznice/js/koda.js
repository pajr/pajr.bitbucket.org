/* global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId="";
  if(stPacienta==1){
    var ime="Matej";
    var priimek="Brudar";
    var rojst="1998-08-28"
  }else if(stPacienta==2){
    var ime="Gal";
    var priimek="Žagar";
    var rojst="1998-08-08"
  }else if(stPacienta==3){
    var ime="Nik";
    var priimek="Gabrič"
    var rojst="1998-12-08"
  }
	var datumInUra = "";
	var telesnaVisina = "";
	var telesnaTeza = "";
	var telesnaTemperatura = "";
	var sistolicniKrvniTlak = "";
	var diastolicniKrvniTlak = "";
	var nasicenostKrviSKisikom = "";
	var merilec = "";
	if(stPacienta==1){
    datumInUra = "2019-02-05T14:30Z";
		telesnaVisina = "182";
  	telesnaTeza = "78.5";
		telesnaTemperatura = "36.85";
		sistolicniKrvniTlak = "120";
    diastolicniKrvniTlak = "88";
		nasicenostKrviSKisikom = "95";
		merilec = "Sestra Liza";
  }else if(stPacienta==2){
		datumInUra = "2019-04-11T22:10Z";
		telesnaVisina = "178";
		telesnaTeza = "75";
		telesnaTemperatura = "37.8";
		sistolicniKrvniTlak = "130";
    diastolicniKrvniTlak = "100";
		nasicenostKrviSKisikom = "80";
		merilec = "Sestra Liza";
  }else if(stPacienta==3){
   	datumInUra = "2019-05-21T14:22Z";
		telesnaVisina = "155";
		telesnaTeza = "72";
		telesnaTemperatura = "35.2";
		sistolicniKrvniTlak = "80";
	  diastolicniKrvniTlak = "50";
		nasicenostKrviSKisikom = "72";
		merilec = "Sestra Liza";
  }
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      success: function (data) {
          var ehrId = data.ehrId;
  
          // build party data
          var partyData = {
              firstNames: ime,
              lastNames: priimek,
              dateOfBirth: rojst,
              partyAdditionalInfo: [
                  {
                      key: "ehrId",
                      value: ehrId
                  }
              ]
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  if (party.action == 'CREATE') {
                      $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		              
                  }
                  $("#preberiEhrIdZaVitalneZnake").append('<option value="'+ehrId+'|'+datumInUra+'|'+telesnaVisina+'|'+telesnaTeza+'|'+telesnaTemperatura+'|'+sistolicniKrvniTlak+'|'+diastolicniKrvniTlak+'|'+nasicenostKrviSKisikom+'|'+merilec+'">'+ime+' '+priimek+'</option>')
                  $("#preberiObstojeciVitalniZnak").append('<option value="'+ehrId+'|'+datumInUra+'|'+telesnaVisina+'|'+telesnaTeza+'|'+telesnaTemperatura+'|'+sistolicniKrvniTlak+'|'+diastolicniKrvniTlak+'|'+nasicenostKrviSKisikom+'|'+merilec+'">'+ime+' '+priimek+'</option>')
                  dodajMeritveVitalnihZnakov(ehrId,datumInUra,telesnaVisina,telesnaTeza,telesnaTemperatura,sistolicniKrvniTlak,diastolicniKrvniTlak,nasicenostKrviSKisikom,merilec);
                
              }
          });
      }
  });
  // TODO: Potrebno implementirati
  return ehrId;
}
function dodajjih(){
  var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();
	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
	  dodajMeritveVitalnihZnakov(ehrId,datumInUra,telesnaVisina,telesnaTeza,telesnaTemperatura,sistolicniKrvniTlak,diastolicniKrvniTlak,nasicenostKrviSKisikom,merilec);
	}
}
function dodajMeritveVitalnihZnakov(ehrId,datumInUra,telesnaVisina,telesnaTeza,telesnaTemperatura,sistolicniKrvniTlak,diastolicniKrvniTlak,nasicenostKrviSKisikom,merilec) {
  $.ajaxSetup({
      "Authorization": getAuthorization()
  });
  var podatki = {
    // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": datumInUra,
      "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
      "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
      "vital_signs/blood_pressure:0/any_event:0/systolic|magnitude":sistolicniKrvniTlak,
      "vital_signs/blood_pressure:0/any_event:0/systolic|unit":"mm[Hg]",
      "vital_signs/blood_pressure:0/any_event:0/diastolic|magnitude":diastolicniKrvniTlak,
      "vital_signs/blood_pressure:0/any_event:0/diastolic|unit":"mm[Hg]",
      "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      
      "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
  };
  var parametriZahteve = {
      ehrId: ehrId,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: merilec
  };
  $.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
        console.log(err.responseText);
      }
  });
}
function pridobipodatek(times,ix){
  
    var ehrId=$("#izpisVitalnoEHR").val();
  	var grr="";
  	var dat="<option value=";
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
});
var searchData = [
    {key: "ehrId", value: ehrId}
];
$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (data) {
      for(var i=0;i<5;i++){
          if(i==0){
            grr="weight";
          }
          if(i==1){
            grr="height";
          }
          if(i==2){
            grr="body_temperature"
          }
          if(i==3){
            grr="blood_pressure"
          }
          if(i==4){
            grr="spO2"
          }
          var count=-1;
        $.ajax({
      				  url: baseUrl + "/view/" + ehrId + "/" + grr,
      			    type: 'GET',
      			    headers: {
                  "Authorization": getAuthorization()
                },
      			    success: function (res) {
      			      count++;
                      switch(count){
                        case 0:
                          dat+=res[ix].temperature+"|";
                          break;
                        case 1:
                          dat+=res[ix].weight+"|";
                          break;
                        case 2:
                          dat+=res[ix].height+"|";
                          break;
                        case 3:
                          dat+=res[ix].systolic+"|";
                          dat+=res[ix].diastolic+"|";
                          break;
                        case 4:
                          dat+=res[ix].spO2;
                          dat+=">"+times+"</option>"
                          zapisneki(dat);
                          return dat;
                          break;
                        default:
                          console.log("fuckyou");
                          break;
                      }
      			    },
      			    error: function() {
      			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
      			    }
    					});
      }
    }
});
}
function zapisneki(dat){
  console.log("tulelelelel:"+dat);
  var podatki=$("#izpisDatum").val()
  console.log(podatki);
  $("#izpisDatum").append(dat);
  
  
}
function pridobidatum(){
  var ehrId=$("#izpisVitalnoEHR").val();
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
});
var searchData = [
    {key: "ehrId", value: ehrId}
];
$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (data) {
      console.log(data);
      $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
                for(var i in res){
                  var datuum=res[i].time;
                  var g=pridobipodatek(datuum,i);
                  console.log(res);
                }
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
    }
});
}
$(document).ready(function() {
  var datee="";
  $("#preberiObstojeciVitalniZnak").change(function(){
    var data=$(this).val().split("|");
    $("#dodajVitalnoEHR").val(data[0]);
  		$("#dodajVitalnoDatumInUra").val(data[1]);
  		$("#dodajVitalnoTelesnaVisina").val(data[2]);
  		$("#dodajVitalnoTelesnaTeza").val(data[3]);
  		$("#dodajVitalnoTelesnaTemperatura").val(data[4]);
  		$("#dodajVitalnoKrvniTlakSistolicni").val(data[5]);
  		$("#dodajVitalnoKrvniTlakDiastolicni").val(data[6]);
  		$("#dodajVitalnoNasicenostKrviSKisikom").val(data[7]);
  		$("#dodajVitalnoMerilec").val(data[8]);
    
  });
  $("#preberiEhrIdZaVitalneZnake").change(function(){
    var podatki=$(this).val().split("|");
    $("#izpisVitalnoEHR").val(podatki[0]);
    $('#izpisDatum')
      .find('option')
      .remove()
      .end()
      .append('<option value=""></option>')
    ;
    pridobidatum();
    
  });
  $('#izpisDatum').change(function() {
  		var podatki = $(this).val().split("|");
  		$("#izpisVitalnoTelesnaVisina").val(podatki[2]);
  		$("#izpisVitalnoTelesnaTeza").val(podatki[1]);
  		$("#izpisVitalnoTelesnaTemperatura").val(podatki[0]);
  		$("#izpisVitalnoKrvniTlakSistolicni").val(podatki[3]);
  		$("#izpisVitalnoKrvniTlakDiastolicni").val(podatki[4]);
  		$("#izpisVitalnoNasicenostKrviSKisikom").val(podatki[5]);
  		$("#izpisVitalnoMerilec").val("Sestra Liza");
  	});
});
function bruh(){
  var tekmeId=[];
  var count=0;
  $.ajax({
      url: "https://www.balldontlie.io/api/v1/games/?page=4753&&per_page=10",
      type: 'GET',
      contentType: 'application/json',
      success: function (data) {
        while(count<3){
          var length=data.data.length;
          tekmeId[count]=data.data[length-1-count].id;
          if(count==0){
          $("#dom1").text(data.data[length-1-count].home_team.full_name)
          $("#gos1").text(data.data[length-1-count].visitor_team.full_name)
          $("#grez1").text(data.data[length-1-count].visitor_team_score)
          $("#drez1").text(data.data[length-1-count].home_team_score)
          $("#dat1").text(parseISOString(data.data[length-1-count].date))
         }
         if(count==1){
          $("#dom2").text(data.data[length-1-count].home_team.full_name)
          $("#gos2").text(data.data[length-1-count].visitor_team.full_name)
          $("#grez2").text(data.data[length-1-count].visitor_team_score)
          $("#drez2").text(data.data[length-1-count].home_team_score)
          $("#dat2").text(parseISOString(data.data[length-1-count].date))
         }
         if(count==2){
          $("#dom3").text(data.data[length-1-count].home_team.full_name)
          $("#gos3").text(data.data[length-1-count].visitor_team.full_name)
          $("#grez3").text(data.data[length-1-count].visitor_team_score)
          $("#drez3").text(data.data[length-1-count].home_team_score)
          $("#dat3").text(parseISOString(data.data[length-1-count].date))
         }
        count++;
        }
        console.log($("#drez1").text())
        var ctxB = document.getElementById("horizontalBar").getContext('2d');
        var chart=new Chart(ctxB, {
    "type": "horizontalBar",
    "data": {
      "labels": [$("#dom1").text(),$("#gos1").text()],
      "datasets": [{
        "label": "Primerjava števila točk",
        "data": [$("#drez1").text(),$("#grez1").text()],
        "fill": false,
        "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)"
        ],
        "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)"
        ],
        "borderWidth": 1
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  });
      }
  });
  console.log($("#dom1").text())
}
function parseISOString(s) {
  var b = s.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija